package runner;



import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
//import cucumber.api.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src\\test\\resources\\features" ,
        glue = "stepDefinition",
        tags = "@test",
        plugin = {
                "json:target/cucumber-reports/cucumber.json",
                "pretty", "html:target/html-report/cucumber-html-reports.html",
                "junit:target/cucumber-reports/cucumber.xml"
        }
        //format = { "pretty", "html:target/site/cucumber-pretty", "json:src/test/resources/json/cucumber.json" }



)
public class TestRunner {

}
