package stepDefinition;



import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinition {

    @Given("This is given")
    public void this_is_given() {
        System.out.println("Inside Given");
    }

    @When("This is when")
    public void this_is_when() {
        System.out.println("Inside When");
    }

    @Then("This is then")
    public void this_is_then() {
        System.out.println("Inside Then");
    }
}

